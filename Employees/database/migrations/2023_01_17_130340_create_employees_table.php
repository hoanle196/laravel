<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->id('id');
            $table->string('department_name');
            $table->string('description');
            $table->timestamps();
        });
        Schema::create('employees', function (Blueprint $table) {
            $table->id('id');
            $table->string('employee_name');
            $table->date('birthday');
            $table->boolean('gender');
            $table->integer('salary');
            $table->unsignedBigInteger('department');
            $table->foreign('department')
                ->references('id')
                ->on('departments')
                ->onDelete('restrict');
            $table->string('img_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
        Schema::dropIfExists('departments');
    }
}
