<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    //
    public function index()
    {
        return view('Admin.users.login', ['title' => 'login']);
    }
    public function create(Request $request)
    {
        return view('Admin.users.register', ['title' => 'Register']);
    }
    public function register(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'email' => 'email|required',
            'password' => 'required|min:3|max:20',
        ]);
        DB::table('users')->insert([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);
        session(['status' => [
            'message' => 'Register successfuly',
            'class' => 'success',
        ]]);
        return redirect('login');
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'email' => 'email|required',
            'password' => 'required|min:3|max:20',
        ]);
        if (
            Auth::attempt([
                'email' => $request->email,
                'password' => $request->password,
            ], $request->remember)
        ) {
            session(['status' => [
                'message' => 'Login success',
                'class' => 'success',

            ], 'login' => true,]);
            return redirect()->route('employee')->with('title', 'abc');
        } else {
            session(['status' => [
                'message' => 'Login fails',
                'class' => 'danger',
            ]]);
            return redirect()->back();
        }
    }
    public function destroy(Request $request)
    {
        if ($request->session()->has('login')) {
            Auth::logout();
            session(['status' => [
                'message' => 'logout successfuly',
                'class' => 'success',
            ]]);
            session()->forget('login');
        }
        return redirect()->route('login');
    }
    public function forgot()
    {
        // $check = User::where('email', 'hoanle196@gmail.com')->first();
        // dd($check);
        return view('Admin.users.forgotPw');
    }
    public function sendEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:users',
        ], [
            'email.required' => 'Email does not empty',
            'email.exists' => 'Email does not exist',
        ]);
        $dataUser = User::where('email', $request->email)->first();
        if ($dataUser) {
            $userId = $dataUser->id;
            $name = $dataUser->name;
            $email = $dataUser->email;
            $token = strtoupper(Str::random(10));
            DB::table('password_resets')->insert([
                'email' => $email,
                'token' => $token,
            ]); //to do something 
            Mail::send('emails.sendPass', compact('email', 'userId', 'name', 'token'), function ($sendEmail) {
                $sendEmail->subject('Change password');
                $sendEmail->to(request()->email);
            });
            session(['status' => [
                'message' => 'Send email successfully, please check Email to change password',
                'class' => 'success',
            ]]);
            return redirect()->route('login');
        } else {
            return session(['status' => [
                'message' => 'send Error',
                'class' => 'danger',
            ]]);
        }
        // dd($check);

    }
    public function changePass(Request $request)
    {

        return view('Admin.users.changPass');
    }
    public function changePassSubmit(Request $request, $userID, $token)
    {
        //validate
        $request->validate([
            'password' => 'required|min:8|max:20',
            'Repassword' => 'required|min:8|max:20|same:password',
        ]);
        //process token 
        //
        //update pass
        User::where('id', $userID)->update([
            'password' => bcrypt($request->password),
        ]);
        session(['status' => [
            'message' => 'Change Password Successfully',
            'class' => 'success',
        ]]);
        return redirect('login');
    }
}
