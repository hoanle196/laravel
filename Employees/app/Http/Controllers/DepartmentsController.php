<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Employee;

class DepartmentsController extends Controller
{
    //
    public function index(Request $request)
    {
        $item = 1;
        $departments = Department::orderBy('created_at', 'DESC')->search()->paginate($item);
        // $departments = Department::paginate($item);
        $pageSum = $departments->lastPage();
        $pageNum = $departments->currentPage();
        $around = 2;
        $next = $pageNum + $around;
        if ($next > $pageSum) {
            $next = $pageSum;
        }
        $pre = $pageNum - $around;
        if ($pre <= 1) $pre = 1;
        $ofset = (request()->input('page', 1) - 1) * $item;
        return view('departments.index', [
            "departments" => $departments ?? '',
            "ofset" => $ofset ?? '',
            "next" => $next ?? '',
            "pre" => $pre ?? '',
        ]);
    }
    public function create(Request $request)
    {
        return view('departments.create');
    }
    public function store(Request $request, Department $departments)
    {
        // dd($request->all());

        $departments->department_name = $request->input('department_name');
        $departments->description = $request->input('description');
        $departments->save();
        session(['status' => [
            'message' => 'Register successfuly',
            'class' => 'success',
        ]]);
        return redirect()->route('department');
    }
    public function edit(Request $request, $id)
    {
        // $departments = Department::all();
        $department = Department::find($id);
        return view('departments.edit', compact('department'));
    }
    public function update(Request $request, Department $department, $id)
    {
        $request->validate([
            'department_name' => 'required',
            'description' => 'required',
        ]);
        $department->where('id', $id)->update([
            'department_name' => $request->department_name,
            'description' => $request->description,
        ]);
        session(['status' => [
            'message' => 'update successfuly',
            'class' => 'success',
        ]]);
        return redirect()->route('department');
    }
    public function destroy(Request $request, Department $department, $id)
    {
        $result = Employee::where('department', $id)->count();
        if ($result > 0) {
            session(['status' => [
                'message' => 'Delete fail, you need to delete the previous employee ',
                'class' => 'danger',
            ]]);
        } else {
            Department::where('id', $id)->delete();
            session(['status' => [
                'message' => 'Delete successfully ',
                'class' => 'success',
            ]]);
        }
        return redirect('department');
    }
}
