<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Department;

class EmployeesController extends Controller
{
    //
    public function index(Request $request, Employee $employee, Department $department)
    {
        $item = 1;
        $employees = Employee::orderBy('created_at', 'DESC')->search()->paginate($item);
        $count = Employee::count();
        // $employees = Employee::paginate($item);
        $pageSum = $employees->lastPage();
        $pageNum = $employees->currentPage();
        $around = 2;
        $next = $pageNum + $around;
        if ($next > $pageSum) {
            $next = $pageSum;
        }
        $pre = $pageNum - $around;
        if ($pre <= 1) $pre = 1;
        $ofset = (request()->input('page', 1) - 1) * $item;
        return view('Employees.index', [
            "employees" => $employees ?? '',
            "ofset" => $ofset ?? '',
            "next" => $next ?? '',
            "pre" => $pre ?? '',
            "count" => $count ?? '',
        ]);
    }
    public function create(Request $request)
    {
        $departments = Department::all();
        $count = Department::count();
        if (Department::count() == 0) {
            session(['status' => [
                'message' => 'Need to have at least 1 departmen',
                'class' => 'warning',
            ]]);
            return redirect()->route('create_department');
        }
        return view('employees.create', compact('departments'));
    }
    public function store(Request $request, Employee $employees)
    {
        // dd($request->all());
        $request->validate([
            'employee_name' => 'required',
            'birthday' => 'required',
            'gender' => 'required',
            'department' => 'required',
            'salary' => 'required|integer',
            'avatar' => 'required|mimes:jpg,png,jpeg|max:5048',
        ]);
        if ($request->file('avatar')->isValid()) {
            $generatedName = 'image' . time() . '-' . $request->avatar->getClientOriginalName();
            $request->avatar->move(public_path('images'), $generatedName);
            $employees->employee_name = $request->input('employee_name');
            $employees->birthday = $request->input('birthday');
            $employees->gender = $request->input('gender');
            $employees->department = $request->input('department');
            $employees->salary = $request->input('salary');
            $employees->img_path = $generatedName;
            $employees->save();
            session(['status' => [
                'message' => 'Register successfuly',
                'class' => 'success',
            ]]);
        }
        // $query = new Employees();
        // $query->create($res);
        // $employees
        // Employees::create(unset($request->all()['_token']));

        return redirect('employee');
    }
    public function edit(Request $request, $id)
    {
        $departments = Department::all();
        $employee = Employee::find($id);
        return view('employees.edit', compact('departments', 'employee'));
    }
    public function show($id)
    {
        $employee = Employee::find($id);
        $department = Department::find($employee->department);
        $employee->department = $department;
        // dd($employee);
        return view('employees.details', compact('employee'));
    }
    public function update(Request $request, Employee $employee, $id)
    {
        $request->validate([
            'employee_name' => 'required',
            'birthday' => 'required',
            'gender' => 'required',
            'department' => 'required',
            'salary' => 'required|integer',
            'avatar' => 'required|mimes:jpg,png,jpeg|max:5048',
        ]);
        if ($request->file('avatar')->isValid()) {
            $generatedName = 'image' . time() . '-' . $request->avatar->getClientOriginalName();
            // dd($generatedName);
            $employee->where('id', $id)->update([
                'employee_name' => $request->employee_name,
                'birthday' => $request->birthday,
                'gender' => $request->gender,
                'department' => $request->department,
                'salary' => $request->salary,
                'img_path' => $generatedName,
            ]);
            session(['status' => [
                'message' => 'update successfuly',
                'class' => 'success',
            ]]);
        }
        return redirect('employee');
    }
    public function destroy(Request $request, Employee $employee, $id)
    {
        $employee->where('id', $id)->delete();
        session(['status' => [
            'message' => 'Delete successfuly',
            'class' => 'success',
        ]]);
        return redirect('employee');
    }
}
