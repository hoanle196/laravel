<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;
    protected $fillable = [
        'department_name',
        'description',
    ];
    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
    public function scopeSearch($query)
    {
        // dd($query);
        if ($keyword = request()->search) {
            return $query->where('department_name', 'like', '%' . $keyword . '%');
        }
    }
}
