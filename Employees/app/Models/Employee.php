<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Employee extends Model
{
    use HasFactory;
    protected $fillable = [
        'employee_name',
        'birthday',
        'gender',
        'salary',
        'department',
    ];
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
    public function scopeSearch($query)
    {
        // dd($query);
        if ($keyword = request()->search) {
            return $query->where('employee_name', 'like', '%' . $keyword . '%');
        }
    }
    // public function getformatDateAttribute()
    // {

    //     return $this->attributes['birthday']->format('d-m-Y');
    // }
    // public function formatSex()
    // {
    // }
}
