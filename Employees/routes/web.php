<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\DepartmentsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::resource('user', UserController::class);
Route::get('login', [LoginController::class, 'index'])->name('login');
Route::post('login', [LoginController::class, 'store'])->name('store');

Route::get('logout', [LoginController::class, 'destroy'])->name('logout');

Route::get('register', [LoginController::class, 'create'])->name('register');
Route::post('register', [LoginController::class, 'register'])->name('register');

Route::get('forgot-password', [LoginController::class, 'forgot'])->name('forgot-password');
Route::post('forgot-password', [LoginController::class, 'sendEmail'])->name('sendEmail');
Route::get('change-Password/{user}/{token}', [LoginController::class, 'changePass'])->name('change-password');
Route::post('change-Password/{user}/{token}', [LoginController::class, 'changePassSubmit'])->name('change-submit');
// employees
Route::middleware(['CustomMiddle'])->group(function () {
    Route::get('employee', [EmployeesController::class, 'index'])->name('employee');
    Route::prefix('employee')->group(function () {
        Route::get('/create', [EmployeesController::class, 'create'])->name('create_employee');
        Route::post('/create', [EmployeesController::class, 'store'])->name('store_employee');
        Route::get('/edit/{id}', [EmployeesController::class, 'edit'])->name('edit_employee');
        Route::get('/detail/{id}', [EmployeesController::class, 'show'])->name('detail_employee');
        Route::put('/update/{id}', [EmployeesController::class, 'update'])->name('update_employee');
        Route::get('/destroy/{id}', [EmployeesController::class, 'destroy'])->name('destroy_employee');
    });
    //departments
    Route::get('department', [DepartmentsController::class, 'index'])->name('department');
    Route::prefix('department')->group(function () {
        Route::get('/create', [DepartmentsController::class, 'create'])->name('create_department');
        Route::post('/create', [DepartmentsController::class, 'store'])->name('store_department');
        Route::get('/edit/{id}', [DepartmentsController::class, 'edit'])->name('edit_department');
        Route::get('/detail/{id}', [DepartmentsController::class, 'show'])->name('detail_department');
        Route::put('/update/{id}', [DepartmentsController::class, 'update'])->name('update_department');
        Route::get('/destroy/{id}', [DepartmentsController::class, 'destroy'])->name('destroy_department');
    });
});
