
@extends('layouts.layoutAdmin.main')
@section ('content')
    @include('layouts.message')
    @include('layouts.layoutAdmin.header')
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Add New Department</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('store_department') }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="card-body">
                    <em class="badge text-danger">
                      @error('department_name')
                          {{ $message }}
                      @enderror
                    </em>
                    <div class="form-group">
                      <label for="department_name">Department name</label>
                      <input type="text" class="form-control" id="department_name" name="department_name" value="{{ old('department_name') }}" placeholder="Enter name">
                    </div>
                    <em class="badge text-danger">
                      @error('description')
                          {{ $message }}
                      @enderror
                    </em>
                    <div class="form-group">
                      <label for="description">Description</label>
                      <textarea name="description" class="form-control" rows="5" id="description"></textarea>
                    </div>
                  </div>
  
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>

    @include('layouts.layoutAdmin.footer')
@endsection















