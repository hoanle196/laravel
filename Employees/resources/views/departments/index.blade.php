
@extends('layouts.layoutAdmin.main')
@section ('content')
    @include('layouts.message')
    @include('layouts.layoutAdmin.header')
        
            <section class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1>List Departments</h1>
                    </div>
                </div>
                </div><!-- /.container-fluid -->
            </section>
            <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- /.row -->
                <div class="row">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-header">
                        <div class="col-2">
                          <a href="{{ route('create_department') }}" type="submit" class="btn btn-primary btn-block">Add new</a>
                        </div>
        
                        <div class="card-tools">
                          <form action="">
                            <div class="input-group input-group-sm" style="width: 150px;">
                              <input type="text" name="search" class="form-control float-right" placeholder="Search">
          
                              <div class="input-group-append">
                                <button type="submit" class="btn btn-default">
                                  <i class="fas fa-search"></i>
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                          <thead>
                            <tr>
                              <th>STT</th>
                              <th>ID</th>
                              <th>Depatment_name</th>
                              <th>Description</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($departments as $department )
                            <tr>
                              <td>{{++$ofset}}</td>
                              <td>{{ $department->id}}</td>
                              <td>{{ $department->department_name}}</td>
                              <td>{{ $department->description }}</td>
                              <td>
                                <a class="btn btn-success" href="{{ route('edit_department', $department->id) }}">Edit</a>
                                <button type="button" data-href="{{ route('destroy_department', $department->id) }}" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                                  Delete
                                </button>
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                            {{ $departments->appends(request()->all())->links('layouts.customPaginate', compact('ofset', 'next', 'pre'))}}
                      </div>
                    </div>
                  </div>
                </div>
              </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->

    @include('layouts.layoutAdmin.footer')
@endsection








