@extends('layouts.layoutLogin.main')
@section ('content')
  <body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="#"><b>Fogot Password</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">
            Enter your registered email to retrieve your password</p>
        <form action=" {{ route('sendEmail') }} " method="post">
          @include('layouts.message')
          @csrf
          {{-- @include('admin.alert') --}}
          <em class="badge text-danger">
            @error('email')
                  {{ $message }}
            @enderror
          </em>
          <div class="input-group mb-3">
            <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
            <!-- /.col -->
            <div class="col-12">
              <button type="submit" class="btn btn-primary btn-block">Send Email</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection



