@extends('layouts.layoutLogin.main')
@section ('content')
  <body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="#"><b>Change Password</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Change to start your session</p>

        <form action="" method="post">
          @include('layouts.message')
          @csrf
          <em class="badge text-danger">
            @error('password')
                  {{ $message }}
            @enderror
          </em>
          <div class="input-group mb-3">
            <input type="password" name="password" class="form-control" placeholder="Password">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <em class="badge text-danger">
            @error('Repassword')
                  {{ $message }}
            @enderror
          </em>
          <div class="input-group mb-3">
            <input type="password" name="Repassword" class="form-control" placeholder="confirm Password">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-4">
              <button type="submit" class="btn btn-primary btn-block">Change</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection



