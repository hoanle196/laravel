
@extends('layouts.layoutAdmin.main')
@section ('content')
    @include('layouts.message')
    @include('layouts.layoutAdmin.header')
            <section class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1>List Employees</h1>
                    </div>
                </div>
                </div><!-- /.container-fluid -->
            </section>
            <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- /.row -->
                <div class="row">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-header">
                        <div class="col-2">
                            <a href="{{ route('create_employee') }}" type="submit" class="btn btn-primary btn-block">Add new</a>
                        </div>
                        <div class=" card-tools">
                          <form action="">
                            <div class="input-group input-group-sm" style="width: 150px;">
                              <input type="text" name="search" class="form-control float-right" placeholder="Search">
          
                              <div class="input-group-append">
                                <button type="submit" class="btn btn-default">
                                  <i class="fas fa-search"></i>
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                          <thead>
                            <tr>
                              <th>STT</th>
                              <th>ID</th>
                              <th>Employee name</th>
                              <th>Day of birth</th>
                              <th>gender</th>
                              <th>department</th>
                              <th>Salary</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @if ( $count == 0)
                            <td style="text-align: center" colspan="12">No result !</td>
                            @endif
                            @foreach ($employees as $employee)
                            <tr>
                              <td>{{ ++$ofset }}</td>
                              <td> {{ $employee->id }}</td>
                              <td><img style="
                                width: 50px;
                                height: 50px;
                                border-radius: 50%;
                                object-fit: cover;" class="avatar" src="{{ URL::asset('images/'.$employee->img_path)}}" alt="avatar"><a href="{{ route('detail_employee', $employee->id) }}">{{ $employee->employee_name }}</a> 
                              </td>
                              <td>{{ $employee->birthday }}</td>
                              <td>{{ $employee->gender }}</td>
                              <td>{{ $employee->department }}</td>
                              <td>{{ $employee->salary }}</td>
                              <td>
                                <a class="btn btn-success" href="{{ route('edit_employee', $employee->id) }}">Edit</a>
                                <button type="button" data-href="{{ route('destroy_employee', $employee->id) }}" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                                  Delete
                                </button>
                              </td>
                              
                              </tr>
                            @endforeach
                            
                          </tbody>
                        </table>
                            {{ $employees->appends(request()->all())->links('layouts.customPaginate', compact('ofset', 'next', 'pre'))}}
                      </div>
                    </div>
                  </div>
                </div>
              </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->

    @include('layouts.layoutAdmin.footer')
@endsection








