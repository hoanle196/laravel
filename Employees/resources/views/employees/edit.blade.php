
@extends('layouts.layoutAdmin.main')
@section ('content')
    @include('layouts.message')
    @include('layouts.layoutAdmin.header')
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Edit Employee</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('update_employee', $employee->id) }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="card-body">
                    <em class="badge text-danger">
                      @error('employee_name')
                          {{ $message }}
                      @enderror
                    </em>
                    <div class="form-group">
                      <label for="employee_name">Employee name</label>
                      <input type="text" class="form-control" id="employee_name" name="employee_name" value="{{ $employee->employee_name ?? old('employee_name') }}" placeholder="Enter name">
                    </div>
                    <em class="badge text-danger">
                      @error('birthday')
                          {{ $message }}
                      @enderror
                    </em>
                    <div class="form-group">
                      <label for="birthday">Date of birth</label>
                      <input type="date" class="form-control" id="birthday" name="birthday" value="{{ $employee->birthday ?? old('birthday') }}" placeholder="Enter birthday">
                    </div>
                    <em class="badge text-danger">
                      @error('gender')
                          {{ $message }}
                      @enderror
                    </em>
                    <div class="form-group">
                      <label for="gender">Gender</label>
                      <select id="gender" name="gender" class="custom-select">
                        <option >Open this select Gender</option>
                        <option {{ $employee->gender == 1 ? 'selected' : ''}} value="1">Male</option>
                        <option {{ $employee->gender == 0 ? 'selected' : ''}} value="0">Female</option>
                      </select>
                    </div>
                    <em class="badge text-danger">
                      @error('department')
                          {{ $message }}
                      @enderror
                    </em>
                    <div class="form-group">
                      <label for="department">Department</label>
                      <select id="department" name="department" class="custom-select">
                        <option >Open this select department</option>
                        @foreach ( $departments as $department )
                          <option {{ $employee->department == $department->id ? 'selected' : ''}} value="{{ $department->id }}">{{ $department->department_name }}</option>
                        @endforeach
                      </select>
                    </div>
                    <em class="badge text-danger">
                      @error('salary')
                          {{ $message }}
                      @enderror
                    </em>
                    <div class="form-group">
                      <label for="salary">Salary</label>
                      <input type="number" class="form-control" id="salary" name="salary" value="{{ $employee->salary ?? old('salary') }}" placeholder="Enter salary">
                    </div>
                    <em class="badge text-danger">
                      @error('avatar')
                          {{ $message }}
                      @enderror
                    </em>

                    <div class="form-group">
                      <label for="avatar">Avatar</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="avatar" name="avatar">
                          <label class="custom-file-label" for="avatar">Choose file</label>
                        </div>
                        <div class="input-group-append">
                          <span class="input-group-text">Upload</span>
                        </div>
                      </div>
                    </div>
                    <img style="
                                width: 50px;
                                height: 50px;
                                border-radius: 50%;
                                object-fit: cover;" class="avatar" src="{{ URL::asset('images/'.$employee->img_path)}}" alt="avatar">
                  </div>
                  <!-- /.card-body -->
  
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a class="btn btn-secondary" href="{{route('employee')}}">Back</a>
                  </div>
                </form>
              </div>


    @include('layouts.layoutAdmin.footer')
@endsection















