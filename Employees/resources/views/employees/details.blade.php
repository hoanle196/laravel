
{{-- <h1>{{ $employee->employee_name}}</h1>
<h1>{{ $employee->birthday}}</h1>
<h1>{{ $employee->gender}}</h1>
<h1>{{ $employee->salary}}</h1>
<h1>{{ $employee->department->department_name}}</h1>
<h1>{{ $employee->department->description}}</h1>
<h1>{{ $employee->img_path}}</h1> --}}


@extends('layouts.layoutAdmin.main')
@section ('content')
    @include('layouts.message')
    @include('layouts.layoutAdmin.header')
            <section class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1>Detail Employee</h1>
                    </div>
                </div>
                </div><!-- /.container-fluid -->
            </section>
            <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- /.row -->
                <div class="row">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-header">
                        <div class="col-2">
                            <a class="btn btn-primary" href="{{route('employee')}}">Back</a>
                        </div>
                        {{-- <div class=" card-tools">
                          <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
        
                            <div class="input-group-append">
                              <button type="submit" class="btn btn-default">
                                <i class="fas fa-search"></i>
                              </button>
                            </div>
                          </div>
                        </div> --}}
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>Employee name</th>
                              <th>Day of birth</th>
                              <th>gender</th>
                              <th>department_id</th>
                              <th>department_name</th>
                              <th>department_description</th>
                              <th>Salary</th>
                            </tr>
                          </thead>
                          <tbody>
                        
                            <tr>
                              <td> {{ $employee->id }}</td>
                              <td><img style="
                                width: 50px;
                                height: 50px;
                                border-radius: 50%;
                                object-fit: cover;" class="avatar" src="{{ URL::asset('images/'.$employee->img_path)}}" alt="avatar"><a href="{{ route('detail_employee', $employee->id) }}">{{ $employee->employee_name }}</a> 
                              </td>
                              <td>{{ $employee->birthday }}</td>
                              <td>{{ $employee->gender }}</td>
                              <td>{{ $employee->department->id }}</td>
                              <td>{{ $employee->department->department_name }}</td>
                              <td>{{ $employee->department->description }}</td>
                              <td>{{ $employee->salary }}</td>
                              </tr>
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->

    @include('layouts.layoutAdmin.footer')
@endsection








