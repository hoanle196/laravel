<!-- jQuery -->
<script src="{{URL::asset('temp/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{URL::asset('temp/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{URL::asset('temp/admin/dist/js/adminlte.min.js')}}"></script>
<script src="{{URL::asset('js/script.js')}}"></script>

</body>
</html>
