@empty (!session('status'))
    <div class="mt-3 alert alert-{{session('status')['class']}}">{{session('status')['message']}}</div>
    {{session()->forget('status')}}        
@endempty